#ifndef ODR_INTERNAL_COMMON_RANDOM_H
#define ODR_INTERNAL_COMMON_RANDOM_H

#include <string>

namespace odr::internal::common {

std::string random_string(std::size_t length);

}

#endif // ODR_INTERNAL_COMMON_RANDOM_H

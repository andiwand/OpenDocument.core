#ifndef ODR_INTERNAL_PROJECT_INFO_H
#define ODR_INTERNAL_PROJECT_INFO_H

namespace odr::internal::project_info {
const char *version() noexcept;
} // namespace odr::internal::project_info

#endif // ODR_INTERNAL_PROJECT_INFO_H

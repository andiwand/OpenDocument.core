#ifndef ODR_INTERNAL_JSON_UTIL_H
#define ODR_INTERNAL_JSON_UTIL_H

#include <iosfwd>

namespace odr::internal::json {

void check_json_file(std::istream &in);

}

#endif // ODR_INTERNAL_JSON_UTIL_H

name: build_test

on: pull_request

jobs:
  build:
    runs-on: ${{ matrix.config.os }}
    strategy:
      fail-fast: false
      matrix:
        config:
          - { os: ubuntu-22.04, cc: clang, cxx: clang++ }
          - { os: ubuntu-22.04, cc: gcc, cxx: g++ }
          - { os: macos-12, cc: clang, cxx: clang++ }
          - { os: windows-2022 }
          - { os: ubuntu-20.04, cc: clang, cxx: clang++ } # TODO remove; but there is no firefox in 22.04 yet

    steps:
      - name: ubuntu install ccache
        if: startsWith(matrix.config.os, 'ubuntu')
        run: |
          sudo apt install ccache
          ccache -V
      - name: macos install ccache
        if: startsWith(matrix.config.os, 'macos')
        run: |
          brew install ccache
          ccache -V

      - name: set up python 3.8
        uses: actions/setup-python@v2
        with:
          python-version: 3.8
      - run: pip install conan

      - name: cache
        uses: actions/cache@v2
        with:
          path: |
            ~/.ccache
            /Users/runner/Library/Caches/ccache
            ~/.conan/data
            C:/.conan
            C:/Users/runneradmin/.conan/data
          key: ${{ matrix.config.os }}-${{ matrix.config.cxx }}-rev18
          restore-keys: |
            ${{ matrix.config.os }}-${{ matrix.config.cxx }}-

      - name: checkout
        uses: actions/checkout@v2

      - name: cmake
        env:
          CC: ${{ matrix.config.cc }}
          CXX: ${{ matrix.config.cxx }}
        run: |
          mkdir -p build
          cd build
          cmake -DCMAKE_BUILD_TYPE=Release ..
      - name: make
        run: |
          cd build
          cmake --build . --config Release

      - name: upload binaries
        uses: actions/upload-artifact@v2
        with:
          name: bin-${{ matrix.config.os }}-${{ matrix.config.cxx }}
          path: |
            build/test/odr_test

  test:
    needs: build
    runs-on: ${{ matrix.config.os }}
    strategy:
      fail-fast: true
      matrix:
        config:
          - { os: ubuntu-20.04, bin: bin-ubuntu-20.04-clang++ }
          - { os: macos-12, bin: bin-macos-12-clang++ }

    steps:
      - name: ubuntu install tidy
        if: startsWith(matrix.config.os, 'ubuntu')
        run: sudo apt install tidy
      - name: macos install tidy
        if: startsWith(matrix.config.os, 'macos')
        run: brew install tidy-html5

      - name: set up python 3.8
        uses: actions/setup-python@v2
        with:
          python-version: 3.8

      - name: checkout
        uses: actions/checkout@v2
        with:
          token: ${{ secrets.PAT_ANDIWAND }}
          submodules: true

      - run: pip install -r test/scripts/requirements.txt

      - name: download binaries
        uses: actions/download-artifact@v2
        with:
          name: ${{ matrix.config.bin }}
          path: build/test
      - name: fix artifact permissions
        run:  chmod +x build/test/odr_test

      - name: test
        working-directory: build/test
        run: ./odr_test

      - name: tidy public test outputs
        run: python3 -u test/scripts/tidy_output.py build/test/output/odr-public/output
      - name: compare public test outputs
        run: python3 -u test/scripts/compare_output.py --driver firefox --max-workers 1 test/data/reference-output/odr-public/output build/test/output/odr-public/output

      - name: tidy private test outputs
        run: python3 -u test/scripts/tidy_output.py build/test/output/odr-private/output
      - name: compare private test outputs
        run: python3 -u test/scripts/compare_output.py --driver firefox --max-workers 1 test/data/reference-output/odr-private/output build/test/output/odr-private/output
